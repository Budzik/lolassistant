namespace LolAssistant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class masteriesChanges : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.MasteryMasteriesTrees", newName: "MasteriesTreeMasteries");
            DropPrimaryKey("dbo.MasteriesTreeMasteries");
            AddPrimaryKey("dbo.MasteriesTreeMasteries", new[] { "MasteriesTree_MasteriesTreeId", "Mastery_MasteryId" });
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.MasteriesTreeMasteries");
            AddPrimaryKey("dbo.MasteriesTreeMasteries", new[] { "Mastery_MasteryId", "MasteriesTree_MasteriesTreeId" });
            RenameTable(name: "dbo.MasteriesTreeMasteries", newName: "MasteryMasteriesTrees");
        }
    }
}

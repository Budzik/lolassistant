﻿using LolAssistant.Exceptions;
using LolAssistant.Models;
using LolAssistant.Models.Entities;
using LolAssistant.Models.ViewModels;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace LolAssistant.Services
{
    public class RiotApiService
    {
        readonly string championUrl = "https://eun1.api.riotgames.com/lol/static-data/v3/champions?champListData=image&api_key=" + WebConfigurationManager.AppSettings["RiotApiKey"].ToString();
        readonly string itemUrl = "https://eun1.api.riotgames.com/lol/static-data/v3/items?itemListData=image&api_key=" + WebConfigurationManager.AppSettings["RiotApiKey"].ToString();
        readonly string summonerSpellUrl = "https://eun1.api.riotgames.com/lol/static-data/v3/summoner-spells?spellListData=image&api_key=" + WebConfigurationManager.AppSettings["RiotApiKey"].ToString();
        readonly string masteriesUrl = "https://eun1.api.riotgames.com/lol/static-data/v3/masteries?masteryListData=all&api_key=" + WebConfigurationManager.AppSettings["RiotApiKey"].ToString();
        readonly string runesUrl = "https://global.api.riotgames.com/api/lol/static-data/EUNE/v1.2/rune?runeListData=image&api_key=" + WebConfigurationManager.AppSettings["RiotApiKey"].ToString();
        readonly string summonerUrl = "https://[SERVER54353533].api.riotgames.com/lol/summoner/v3/summoners/by-name/[6456353NICKNAME]?api_key=" + WebConfigurationManager.AppSettings["RiotApiKey"].ToString();
        readonly string matchUrL = "https://[SERVER54353533].api.riotgames.com/lol/spectator/v3/active-games/by-summoner/[SUMMONERSID923048]?api_key=" + WebConfigurationManager.AppSettings["RiotApiKey"].ToString();
        readonly string nickNameUniqueString = "[6456353NICKNAME]";
        readonly string newServerString = "[SERVER54353533]";
        readonly string summonerIdString = "[SUMMONERSID923048]";

        private LolAssistantContext DatabaseContext;

        public RiotApiService()
        {
            DatabaseContext = new LolAssistantContext();
        }
        public bool IsMatchFound(int id, Servers server)
        {
            try
            {
                string response = GetTextResponse(matchUrL.Replace(summonerIdString, id.ToString()).Replace(newServerString, GetNewServerText(server)));
                JObject serializedText = JObject.Parse(response);
                long result = serializedText.Value<long>("gameId");
                return true;
            }
            catch
            {
                return false;
            }
        }
        public MatchVM GetSecretTeam()
        {
            MatchVM match = new MatchVM() { Team1 = getSecretFirstTeam(), Team2 = getSecretSecondTeam(), server = Servers.PBE };
            Random rand = new Random(DateTime.Now.Second);
            int sum = rand.Next(0, 5);
            int team = rand.Next(1, 3);
            if (team == 1)
            {
                match.Team1.Summoners.ElementAt(sum).IsItYou = true;
            }
            else
            {
                match.Team2.Summoners.ElementAt(sum).IsItYou = true;
            }
            SetTipsToMatch(match);
            return match;
        }
        private TeamVM getSecretFirstTeam()
        {
            TeamVM team = new TeamVM() { TeamSite = TeamSite.Blue, Summoners = getFirstTeamSummoners(), Bans = getBans() };
            return team;
        }
        private TeamVM getSecretSecondTeam()
        {
            TeamVM team = new TeamVM() { TeamSite = TeamSite.Red, Summoners = getSecondTeamSummoners(), Bans = getBans() };
            return team;
        }
        private List<SummonerVM> getFirstTeamSummoners()
        {
            List<SummonerVM> summoners = new List<SummonerVM>();
            ChampionService cs = new ChampionService();
            SummonerSpellService sss = new SummonerSpellService();
            summoners.Add(new SummonerVM() { Champion = cs.GetChampionByName("Garen"), Masteries = new List<MasteryVM>(), Name = "A666A", Runes = new List<RuneVM>(), SumSpell1 = sss.GetSummonersSpellsById(1), SumSpell2 = sss.GetSummonersSpellsById(12), IsItYou = false });
            summoners.Add(new SummonerVM() { Champion = cs.GetChampionByName("Katarina"), Masteries = new List<MasteryVM>(), Name = "B303B", Runes = new List<RuneVM>(), SumSpell1 = sss.GetSummonersSpellsById(1), SumSpell2 = sss.GetSummonersSpellsById(12), IsItYou = false });
            summoners.Add(new SummonerVM() { Champion = cs.GetChampionByName("Lee Sin"), Masteries = new List<MasteryVM>(), Name = "JJJJJ", Runes = new List<RuneVM>(), SumSpell1 = sss.GetSummonersSpellsById(1), SumSpell2 = sss.GetSummonersSpellsById(12), IsItYou = false });
            summoners.Add(new SummonerVM() { Champion = cs.GetChampionByName("Blitzcrank"), Masteries = new List<MasteryVM>(), Name = "WWSSA", Runes = new List<RuneVM>(), SumSpell1 = sss.GetSummonersSpellsById(1), SumSpell2 = sss.GetSummonersSpellsById(12), IsItYou = false });
            summoners.Add(new SummonerVM() { Champion = cs.GetChampionByName("Sivir"), Masteries = new List<MasteryVM>(), Name = "GGGGG", Runes = new List<RuneVM>(), SumSpell1 = sss.GetSummonersSpellsById(1), SumSpell2 = sss.GetSummonersSpellsById(12), IsItYou = false });

            return summoners;
        }
        private List<SummonerVM> getSecondTeamSummoners()
        {
            List<SummonerVM> summoners = new List<SummonerVM>();
            ChampionService cs = new ChampionService();
            SummonerSpellService sss = new SummonerSpellService();
            summoners.Add(new SummonerVM() { Champion = cs.GetChampionByName("Pantheon"), Masteries = new List<MasteryVM>(), Name = "A667A", Runes = new List<RuneVM>(), SumSpell1 = sss.GetSummonersSpellsById(1), SumSpell2 = sss.GetSummonersSpellsById(12), IsItYou = false });
            summoners.Add(new SummonerVM() { Champion = cs.GetChampionByName("Malzahar"), Masteries = new List<MasteryVM>(), Name = "B307B", Runes = new List<RuneVM>(), SumSpell1 = sss.GetSummonersSpellsById(1), SumSpell2 = sss.GetSummonersSpellsById(12), IsItYou = false });
            summoners.Add(new SummonerVM() { Champion = cs.GetChampionByName("Amumu"), Masteries = new List<MasteryVM>(), Name = "C4044", Runes = new List<RuneVM>(), SumSpell1 = sss.GetSummonersSpellsById(1), SumSpell2 = sss.GetSummonersSpellsById(12), IsItYou = false });
            summoners.Add(new SummonerVM() { Champion = cs.GetChampionByName("Thresh"), Masteries = new List<MasteryVM>(), Name = "ABCDS", Runes = new List<RuneVM>(), SumSpell1 = sss.GetSummonersSpellsById(1), SumSpell2 = sss.GetSummonersSpellsById(12), IsItYou = false });
            summoners.Add(new SummonerVM() { Champion = cs.GetChampionByName("Vayne"), Masteries = new List<MasteryVM>(), Name = "GGGGG", Runes = new List<RuneVM>(), SumSpell1 = sss.GetSummonersSpellsById(1), SumSpell2 = sss.GetSummonersSpellsById(12), IsItYou = false });

            return summoners;
        }
        private List<Champion> getBans()
        {
            List<Champion> bans = new List<Champion>();
            ChampionService cs = new ChampionService();
            bans.Add(cs.GetChampionByName("Yorick"));
            bans.Add(cs.GetChampionByName("Yorick"));
            bans.Add(cs.GetChampionByName("Yorick"));
            bans.Add(cs.GetChampionByName("Yorick"));
            bans.Add(cs.GetChampionByName("Yorick"));
            return bans;
        }
        public void RefreshChampionList()
        {
            string response = GetTextResponse(championUrl);
            List<Champion> championList = GetAllChampionsFromText(response);
            foreach (var item in DatabaseContext.Champions)
            {
                DatabaseContext.Champions.Remove(item);
            }
            foreach (var item in championList)
            {
                DatabaseContext.Champions.Add(item);
            }
            DatabaseContext.SaveChanges();
        }
        public void RefreshItemList()
        {
            string response = GetTextResponse(itemUrl);
            List<Item> itemList = GetAllItemsFromText(response);
            foreach (var item in DatabaseContext.Items)
            {
                DatabaseContext.Items.Remove(item);
            }
            foreach (var item in itemList)
            {
                DatabaseContext.Items.Add(item);
            }
            DatabaseContext.SaveChanges();
        }
        public void RefreshMasteriesList()
        {
            string response = GetTextResponse(masteriesUrl);
            List<Mastery> masteryList = GetAllMasteriesFromText(response);
            foreach (var item in DatabaseContext.Masteries)
            {
                DatabaseContext.Masteries.Remove(item);
            }
            foreach (var item in masteryList)
            {
                DatabaseContext.Masteries.Add(item);
            }
            DatabaseContext.SaveChanges();
        }
        public void RefreshSummonersSpellsList()
        {
            string response = GetTextResponse(summonerSpellUrl);
            List<SummonersSpell> summonersSpellList = GetAllSummonersSpellsFromText(response);
            foreach (var item in DatabaseContext.SummonersSpells)
            {
                DatabaseContext.SummonersSpells.Remove(item);
            }
            foreach (var item in summonersSpellList)
            {
                DatabaseContext.SummonersSpells.Add(item);
            }
            DatabaseContext.SaveChanges();
        }
        public void RefreshRunesList()
        {
            string response = GetTextResponse(runesUrl);
            List<Rune> runeList = GetAllRunesFromText(response);
            foreach (var item in DatabaseContext.Runes)
            {
                DatabaseContext.Runes.Remove(item);
            }
            foreach (var item in runeList)
            {
                DatabaseContext.Runes.Add(item);
            }
            DatabaseContext.SaveChanges();
        }
        public MatchVM GetMatchBySummonerName(string name, Servers server)
        {
            int id = GetSummonerIdFromName(name, server);
            if (id == -1)
            {
                throw new PlayerNotFoundException("Player Not Found");
            }
            else
            {
                try
                {
                    MatchVM match = GetMatchFromId(id, name, server);
                    match.server = server;
                    SetTipsToMatch(match);
                    return match;
                }
                catch (Exception e)
                {
                    throw new MatchNotFoundException("Match not found.");
                }

            }

        }
        private int GetSummonerIdFromName(string name, Servers server)
        {
            try
            {
                string response = GetTextResponse(summonerUrl.Replace(nickNameUniqueString, name).Replace(newServerString, GetNewServerText(server)));
                JObject serializedText = JObject.Parse(response);
                List<JToken> result = serializedText.Children().ToList();
                return result[0].ToObject<int>();
            }
            catch
            {
                return -1;
            }
        }
        private MatchVM GetMatchFromId(int id, string name, Servers server)
        {
            string response = GetTextResponse(matchUrL.Replace(summonerIdString, id.ToString()).Replace(newServerString, GetNewServerText(server)));
            return GetMatchFromResponse(name, response);
        }
        private MatchVM GetMatchFromResponse(string name, string response)
        {
            MatchVM match = new MatchVM();
            match.Team1 = GetTeamFromText(100, name, response);
            match.Team2 = GetTeamFromText(200, name, response);
            SetTipsToMatch(match);
            return match;
        }
        private void SetTipsToMatch(MatchVM match)
        {
            match.tips = GetTipsByCounteredChampion(match);
        }
        private List<TipVM> GetTipsByCounteredChampion(MatchVM match)
        {
            List<TipVM> tips;
            Champion champion = getFoundChampion(match);
            List<Champion> oppositeChampions = getOppositeChampions(match);
            tips = getTipsFromChampionAndOppositeChampions(champion, oppositeChampions);
            return tips;
        }
        private Champion getFoundChampion(MatchVM match)
        {
            foreach (var item in match.Team1.Summoners)
            {
                if (item.IsItYou)
                {
                    return item.Champion;
                }
            }
            foreach (var item in match.Team2.Summoners)
            {
                if (item.IsItYou)
                {
                    return item.Champion;
                }
            }
            throw new ArgumentException("Match not contain found summoner.");
        }
        private List<Champion> getOppositeChampions(MatchVM match)
        {
            List<Champion> champions = new List<Champion>();
            foreach (var item in match.Team1.Summoners)
            {
                if (item.IsItYou)
                {
                    foreach (var item1 in match.Team2.Summoners)
                    {
                        champions.Add(item1.Champion);
                    }
                }
            }
            foreach (var item in match.Team2.Summoners)
            {
                if (item.IsItYou)
                {
                    foreach (var item1 in match.Team1.Summoners)
                    {
                        champions.Add(item1.Champion);
                    }
                }
            }
            return champions;
            throw new ArgumentException("Match not contain found summoner.");
        }
        private List<TipVM> getTipsFromChampionAndOppositeChampions(Champion currentChampion, List<Champion> oppositeChampions)
        {
            List<TipVM> tips = new List<TipVM>();
            ChampionService cs = new ChampionService();
            foreach (var item in oppositeChampions)
            {
                tips.AddRange(cs.GetTipsFromCounteredChampions(currentChampion, item));
            }
            return tips;
        }
        private List<string> GetTipsByItems(SummonerVM summoner, MatchVM match)
        {
            List<string> tips = new List<string>();
            return tips;
        }
        private TeamVM GetTeamFromText(int teamId, string name, string response)
        {
            TeamVM team = new TeamVM();
            List<Champion> bansList = new List<Champion>();
            List<SummonerVM> summonersList = new List<SummonerVM>();
            JObject serializedText = JObject.Parse(response);
            List<JToken> participants = serializedText["participants"].Children().ToList();
            List<JToken> bans = serializedText["bannedChampions"].Children().ToList();
            SummonerSpellService summonerSpellService = new SummonerSpellService();
            ChampionService championService = new ChampionService();
            RuneService runeService = new RuneService();
            foreach (var item in participants)
            {
                if (item["teamId"].ToObject<int>() == teamId)
                {
                    summonersList.Add(new SummonerVM()
                    {
                        IsItYou = item["summonerName"].ToObject<string>().Equals(name),
                        Name = item["summonerName"].ToObject<string>(),
                        Champion = championService.GetChampionByLolId(item["championId"].ToObject<int>()),
                        SumSpell1 = summonerSpellService.GetSummonersSpellsById(item["spell1Id"].ToObject<int>()),
                        SumSpell2 = summonerSpellService.GetSummonersSpellsById(item["spell2Id"].ToObject<int>()),
                        Masteries = GetMasteriesFromJToken(item),
                        Runes = GetRunesFromJToken(item)
                        
                    });
                }
            }
            foreach (var item in bans)
            {
                if (item["teamId"].ToObject<int>() == teamId)
                {
                    bansList.Add(championService.GetChampionByLolId(item["championId"].ToObject<int>()));
                }
            }
            team.TeamSite = GetTeamSiteFromId(teamId);
            team.Summoners = summonersList;
            team.Bans = bansList;
            return team;
        }
        private IEnumerable<MasteryVM> GetMasteriesFromJToken(JToken token)
        {
            List<MasteryVM> masteries = new List<MasteryVM>();
            JEnumerable<JToken> JTokens = token["masteries"].Children();
            MasteryService masteryService = new MasteryService();
            foreach (var item in JTokens)
            {
                masteries.Add(new MasteryVM() { count = item["rank"].ToObject<int>(), mastery = masteryService.GetMasteryByLolId(item["masteryId"].ToObject<int>()) });
            }
            return masteries;
        }
        private IEnumerable<RuneVM> GetRunesFromJToken(JToken token)
        {
            List<RuneVM> masteries = new List<RuneVM>();
            JEnumerable<JToken> JTokens = token["runes"].Children();
            RuneService runeService = new RuneService();
            foreach (var item in JTokens)
            {
                masteries.Add(new RuneVM() { Count = item["count"].ToObject<int>(), Rune = runeService.GetRuneByLolId(item["runeId"].ToObject<int>()) });
            }
            return masteries;
        }
        private TeamSite GetTeamSiteFromId(int id)
        {
            if (id == 100)
            {
                return TeamSite.Blue;
            }
            else
            {
                return TeamSite.Red;
            }
        }
        private string GetNewServerText(Servers server)
        {
            switch (server)
            {
                case Servers.EUNE:
                    return "EUN1";
                    break;
                case Servers.EUW:
                    return "EUW1";
                    break;
                case Servers.BR:
                    return "BR1";
                    break;
                case Servers.LAN:
                    return "LA1";
                    break;
                case Servers.LAS:
                    return "LA2";
                    break;
                case Servers.NA:
                    return "NA1";
                    break;
                case Servers.OCE:
                    return "OC1";
                    break;
                case Servers.RU:
                    return "RU";
                    break;
                case Servers.TR:
                    return "TR1";
                    break;
                case Servers.SEA:
                    return server.ToString();
                    break;
                case Servers.KR:
                    return "KR";
                    break;
                case Servers.PBE:
                    return server.ToString();
                    break;
                default:
                    return server.ToString();
                    break;
            }
        }
        private List<Champion> GetAllChampionsFromText(string text)
        {
            List<Champion> champs = new List<Champion>();
            JObject serializedText = JObject.Parse(text);
            List<JToken> results = serializedText["data"].Children().Children().ToList();
            foreach (var item in results)
            {
                champs.Add(new Champion() { Name = item["name"].ToObject<string>(), Title = item["title"].ToObject<string>(), LolChampionId = item["id"].ToObject<int>(), Graphic = item["image"]["full"].ToObject<string>() });
            }
            return champs;
        }
        private List<Item> GetAllItemsFromText(string text)
        {
            List<Item> items = new List<Item>();
            JObject serializedText = JObject.Parse(text);
            List<JToken> results = serializedText["data"].Children().Children().ToList();
            foreach (var item in results)
            {
                try
                {
                    items.Add(new Item { Name = item["name"].ToObject<string>(), Description = item["description"].ToObject<string>(), LolItemId = item["id"].ToObject<int>(), Graphic = item["image"]["full"].ToObject<string>() });
                }
                catch (Exception e)
                {

                }

            }
            return items;
        }
        private List<Mastery> GetAllMasteriesFromText(string text)
        {
            List<Mastery> items = new List<Mastery>();
            JObject serializedText = JObject.Parse(text);
            List<JToken> results = serializedText["data"].Children().Children().ToList();
            foreach (var item in results)
            {
                try
                {
                    items.Add(new Mastery
                    {
                        Name = item["name"].ToObject<string>(),
                        LolMasteryId = item["id"].ToObject<int>(),
                        Graphic = item["image"]["full"].ToObject<string>(),
                        Profit = item["description"].ToList()[0].ToObject<string>(),
                        MasteryTree = GetMasteryTreeFromText(item["masteryTree"].ToObject<string>()),
                        HorizontalPosition = GetMasteryHorizontalPositionFromId(item["id"].ToObject<int>()),
                        VerticalPosition = GetMasteryVerticalPositionFromId(item["id"].ToObject<int>())
                    });
                }
                catch (Exception e)
                {

                }

            }
            return items;
        }
        private List<SummonersSpell> GetAllSummonersSpellsFromText(string text)
        {
            List<SummonersSpell> items = new List<SummonersSpell>();
            JObject serializedText = JObject.Parse(text);
            List<JToken> results = serializedText["data"].Children().Children().ToList();
            foreach (var item in results)
            {
                try
                {
                    items.Add(new SummonersSpell { Name = item["name"].ToObject<string>(), LolSummonersSpellId = item["id"].ToObject<int>(), Graphic = item["image"]["full"].ToObject<string>() });
                }
                catch (Exception e)
                {

                }

            }
            return items;
        }
        private List<Rune> GetAllRunesFromText(string text)
        {
            List<Rune> items = new List<Rune>();
            JObject serializedText = JObject.Parse(text);
            List<JToken> results = serializedText["data"].Children().Children().ToList();
            foreach (var item in results)
            {
                items.Add(new Rune
                {
                    Name = item["name"].ToObject<string>(),
                    LolRuneId = item["id"].ToObject<int>(),
                    Tier = item["rune"]["tier"].ToObject<int>(),
                    Graphic = item["image"]["full"].ToObject<string>(),
                    Type = item["rune"]["type"].ToObject<string>().Equals("red") ? RuneType.Mark : item["rune"]["type"].ToObject<string>().Equals("yellow") ? RuneType.Seal : item["rune"]["type"].ToObject<string>().Equals("blue") ? RuneType.Glyph : item["rune"]["type"].ToObject<string>().Equals("black") ? RuneType.Quintessence : RuneType.Quintessence,
                    Profit = GetProfitFromText(item["description"].ToObject<string>()),
                    Value = GetValueFromText(item["description"].ToObject<string>())
                });


            }
            return items;
        }
        private string GetTextResponse(string url)
        {
            WebRequest request = WebRequest.Create(url);
            WebResponse response = request.GetResponse();
            using (Stream stream = response.GetResponseStream())
            {
                StreamReader reader = new StreamReader(stream);
                string text = reader.ReadToEnd();
                return text;
            }
        }
        private float GetValueFromText(string text)
        {
            string profitText = text.Split(' ')[0];
            if (profitText[0] == '+')
            {
                profitText = profitText.Remove(0, 1);
            }
            if (profitText[profitText.Length - 1] == '%')
            {
                profitText = profitText.Remove(profitText.Length - 1, 1);
            }
            return float.Parse(profitText);
        }
        private string GetProfitFromText(string text)
        {
            string[] profitText = text.Split(' ');
            StringBuilder returnedText = new StringBuilder();
            int i = 0;
            foreach (var item in profitText)
            {
                if (i != 0)
                {
                    returnedText.Append(item);
                    returnedText.Append(' ');
                }
                i++;
            }
            return returnedText.ToString();
        }
        private MasteryTree GetMasteryTreeFromText(string text)
        {
            if (text.Equals("Ferocity"))
            {
                return MasteryTree.Ferocity;
            }
            else if (text.Equals("Cunning"))
            {
                return MasteryTree.Cunning;
            }
            else if (text.Equals("Resolve"))
            {
                return MasteryTree.Resolve;
            }
            throw new ArgumentException("Wrong mastery tree name");
        }
        private int GetMasteryHorizontalPositionFromId(int id)
        {
            int position = id % 10;
            int verticalPosition = (id % 100) / 10;
            if (position == 4 && verticalPosition % 2 == 1)
            {
                return 2;
            }
            else if (position == 4 && verticalPosition % 2 == 0)
            {
                return 3;
            }
            else
            {
                return position;
            }
        }
        private int GetMasteryVerticalPositionFromId(int id)
        {
            int position = (id % 100) / 10;
            return position;
        }
    }
}

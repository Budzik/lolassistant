﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LolAssistant.Models.Entities
{
    class CounterChampion
    {
        public int CounterChampionId { get; set; }
        public string Description { get; set; }
        public Champion CounteredChampion { get; set; }
        public Champion CounteringChampion { get; set; }
    }
}

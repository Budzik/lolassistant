﻿using LolAssistant.Models;
using LolAssistant.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LolAssistant.Services
{
    public class SummonerSpellService
    {
        private LolAssistantContext Context;

        public SummonerSpellService()
        {
            Context = new LolAssistantContext();
        }
        public IEnumerable<SummonersSpell> GetAllSummonersSpells()
        {
            return Context.SummonersSpells.ToList();
        }

        public SummonersSpell GetSummonersSpellsById(int id)
        {
            try
            {
                return (from c in Context.SummonersSpells where c.LolSummonersSpellId == id select c).ToList()[0];
            }
            catch (Exception e)
            {
                throw new ArgumentException("Summoner spell with " + id.ToString() + " lol id not found.");
            }
        }
    }
}

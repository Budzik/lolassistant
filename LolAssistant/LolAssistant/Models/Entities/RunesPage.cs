﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LolAssistant.Models.Entities
{
    public class RunesPage
    {
        public int RunesPageId { get; set; }
        public virtual ICollection<Rune> Runes { get; set; }
        public virtual IEnumerable<SynergisticRunesPage> SynergisticRunesPage { get; set; }

        public RunesPage()
        {
            Runes = new HashSet<Rune>();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LolAssistant.Models.Entities
{
    class SynergisticChampion
    {
        public int SynergisticChampionId { get; set; }
        public string Description { get; set; }
        public virtual Champion FirstChampion { get; set; }
        public virtual Champion SecondChampion { get; set; }
    }
}

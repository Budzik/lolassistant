﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LolAssistant.Models.Entities
{
    public class SynergisticItem
    {
        public int SynergisticItemId { get; set; }
        public string Description { get; set; }
        public virtual Champion Champions { get; set; }
        public virtual Item Items { get; set; }
    }
}

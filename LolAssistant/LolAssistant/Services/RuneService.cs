﻿using LolAssistant.Models;
using LolAssistant.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LolAssistant.Services
{
    public class RuneService
    {
        private LolAssistantContext Context;

        public RuneService()
        {
            Context = new LolAssistantContext();
        }
        IEnumerable<Rune> GetAllRunes()
        {
            return Context.Runes.ToList();
        }

        public Rune GetRuneByLolId(int id)
        {
            try
            {
                return (from c in Context.Runes where c.LolRuneId == id select c).ToList()[0];
            }
            catch (Exception e)
            {
                throw new ArgumentException("Rune with " + id.ToString() + " lol id not found.");
            }
        }
    }
}

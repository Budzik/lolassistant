namespace LolAssistant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDescriptionFieldInSynegisticAndCounterEntities : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CounterChampions", "Description", c => c.String());
            AddColumn("dbo.CounterItems", "Description", c => c.String());
            AddColumn("dbo.CounterSummonersSpells", "Description", c => c.String());
            AddColumn("dbo.SynergisticChampions", "Description", c => c.String());
            AddColumn("dbo.SynergisticItems", "Description", c => c.String());
            AddColumn("dbo.SynergisticMasteriesTrees", "Description", c => c.String());
            AddColumn("dbo.SynergisticRunesPages", "Description", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.SynergisticRunesPages", "Description");
            DropColumn("dbo.SynergisticMasteriesTrees", "Description");
            DropColumn("dbo.SynergisticItems", "Description");
            DropColumn("dbo.SynergisticChampions", "Description");
            DropColumn("dbo.CounterSummonersSpells", "Description");
            DropColumn("dbo.CounterItems", "Description");
            DropColumn("dbo.CounterChampions", "Description");
        }
    }
}

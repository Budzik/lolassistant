﻿using System;

namespace LolAssistant.Exceptions
{
    class PlayerNotFoundException : Exception
    {
        public PlayerNotFoundException(string message) : base(message)
        {            
        }
    }
}

// <auto-generated />
namespace LolAssistant.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddTierToRunes : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddTierToRunes));
        
        string IMigrationMetadata.Id
        {
            get { return "201705071523059_AddTierToRunes"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}

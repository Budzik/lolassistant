namespace LolAssistant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class repairDataBase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MasteriesTrees",
                c => new
                    {
                        MasteriesTreeId = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.MasteriesTreeId);
            
            AddColumn("dbo.CounterChampions", "CounteredChampion_ChampionId", c => c.Int());
            AddColumn("dbo.CounterChampions", "CounteringChampion_ChampionId", c => c.Int());
            AddColumn("dbo.CounterItems", "Champion_ChampionId", c => c.Int());
            AddColumn("dbo.CounterItems", "Item_ItemId", c => c.Int());
            AddColumn("dbo.CounterSummonersSpells", "Champion_ChampionId", c => c.Int());
            AddColumn("dbo.CounterSummonersSpells", "SummonersSpell_SummonersSpellId", c => c.Int());
            AddColumn("dbo.RunesPages", "Rune_RuneId", c => c.Int());
            AddColumn("dbo.SynergisticChampions", "FirstChampion_ChampionId", c => c.Int());
            AddColumn("dbo.SynergisticChampions", "SecondChampion_ChampionId", c => c.Int());
            AddColumn("dbo.SynergisticItems", "Champions_ChampionId", c => c.Int());
            AddColumn("dbo.SynergisticItems", "Items_ItemId", c => c.Int());
            AddColumn("dbo.SynergisticMasteriesTrees", "Champion_ChampionId", c => c.Int());
            AddColumn("dbo.SynergisticMasteriesTrees", "MasteriesTree_MasteriesTreeId", c => c.Int());
            AddColumn("dbo.SynergisticRunesPages", "Champion_ChampionId", c => c.Int());
            AddColumn("dbo.SynergisticRunesPages", "RunesPage_RunesPageId", c => c.Int());
            CreateIndex("dbo.CounterChampions", "CounteredChampion_ChampionId");
            CreateIndex("dbo.CounterChampions", "CounteringChampion_ChampionId");
            CreateIndex("dbo.CounterItems", "Champion_ChampionId");
            CreateIndex("dbo.CounterItems", "Item_ItemId");
            CreateIndex("dbo.CounterSummonersSpells", "Champion_ChampionId");
            CreateIndex("dbo.CounterSummonersSpells", "SummonersSpell_SummonersSpellId");
            CreateIndex("dbo.RunesPages", "Rune_RuneId");
            CreateIndex("dbo.SynergisticChampions", "FirstChampion_ChampionId");
            CreateIndex("dbo.SynergisticChampions", "SecondChampion_ChampionId");
            CreateIndex("dbo.SynergisticItems", "Champions_ChampionId");
            CreateIndex("dbo.SynergisticItems", "Items_ItemId");
            CreateIndex("dbo.SynergisticMasteriesTrees", "Champion_ChampionId");
            CreateIndex("dbo.SynergisticMasteriesTrees", "MasteriesTree_MasteriesTreeId");
            CreateIndex("dbo.SynergisticRunesPages", "Champion_ChampionId");
            CreateIndex("dbo.SynergisticRunesPages", "RunesPage_RunesPageId");
            AddForeignKey("dbo.CounterChampions", "CounteredChampion_ChampionId", "dbo.Champions", "ChampionId");
            AddForeignKey("dbo.CounterChampions", "CounteringChampion_ChampionId", "dbo.Champions", "ChampionId");
            AddForeignKey("dbo.CounterItems", "Champion_ChampionId", "dbo.Champions", "ChampionId");
            AddForeignKey("dbo.CounterItems", "Item_ItemId", "dbo.Items", "ItemId");
            AddForeignKey("dbo.CounterSummonersSpells", "Champion_ChampionId", "dbo.Champions", "ChampionId");
            AddForeignKey("dbo.CounterSummonersSpells", "SummonersSpell_SummonersSpellId", "dbo.SummonersSpells", "SummonersSpellId");
            AddForeignKey("dbo.RunesPages", "Rune_RuneId", "dbo.Runes", "RuneId");
            AddForeignKey("dbo.SynergisticChampions", "FirstChampion_ChampionId", "dbo.Champions", "ChampionId");
            AddForeignKey("dbo.SynergisticChampions", "SecondChampion_ChampionId", "dbo.Champions", "ChampionId");
            AddForeignKey("dbo.SynergisticItems", "Champions_ChampionId", "dbo.Champions", "ChampionId");
            AddForeignKey("dbo.SynergisticItems", "Items_ItemId", "dbo.Items", "ItemId");
            AddForeignKey("dbo.SynergisticMasteriesTrees", "Champion_ChampionId", "dbo.Champions", "ChampionId");
            AddForeignKey("dbo.SynergisticMasteriesTrees", "MasteriesTree_MasteriesTreeId", "dbo.MasteriesTrees", "MasteriesTreeId");
            AddForeignKey("dbo.SynergisticRunesPages", "Champion_ChampionId", "dbo.Champions", "ChampionId");
            AddForeignKey("dbo.SynergisticRunesPages", "RunesPage_RunesPageId", "dbo.RunesPages", "RunesPageId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SynergisticRunesPages", "RunesPage_RunesPageId", "dbo.RunesPages");
            DropForeignKey("dbo.SynergisticRunesPages", "Champion_ChampionId", "dbo.Champions");
            DropForeignKey("dbo.SynergisticMasteriesTrees", "MasteriesTree_MasteriesTreeId", "dbo.MasteriesTrees");
            DropForeignKey("dbo.SynergisticMasteriesTrees", "Champion_ChampionId", "dbo.Champions");
            DropForeignKey("dbo.SynergisticItems", "Items_ItemId", "dbo.Items");
            DropForeignKey("dbo.SynergisticItems", "Champions_ChampionId", "dbo.Champions");
            DropForeignKey("dbo.SynergisticChampions", "SecondChampion_ChampionId", "dbo.Champions");
            DropForeignKey("dbo.SynergisticChampions", "FirstChampion_ChampionId", "dbo.Champions");
            DropForeignKey("dbo.RunesPages", "Rune_RuneId", "dbo.Runes");
            DropForeignKey("dbo.CounterSummonersSpells", "SummonersSpell_SummonersSpellId", "dbo.SummonersSpells");
            DropForeignKey("dbo.CounterSummonersSpells", "Champion_ChampionId", "dbo.Champions");
            DropForeignKey("dbo.CounterItems", "Item_ItemId", "dbo.Items");
            DropForeignKey("dbo.CounterItems", "Champion_ChampionId", "dbo.Champions");
            DropForeignKey("dbo.CounterChampions", "CounteringChampion_ChampionId", "dbo.Champions");
            DropForeignKey("dbo.CounterChampions", "CounteredChampion_ChampionId", "dbo.Champions");
            DropIndex("dbo.SynergisticRunesPages", new[] { "RunesPage_RunesPageId" });
            DropIndex("dbo.SynergisticRunesPages", new[] { "Champion_ChampionId" });
            DropIndex("dbo.SynergisticMasteriesTrees", new[] { "MasteriesTree_MasteriesTreeId" });
            DropIndex("dbo.SynergisticMasteriesTrees", new[] { "Champion_ChampionId" });
            DropIndex("dbo.SynergisticItems", new[] { "Items_ItemId" });
            DropIndex("dbo.SynergisticItems", new[] { "Champions_ChampionId" });
            DropIndex("dbo.SynergisticChampions", new[] { "SecondChampion_ChampionId" });
            DropIndex("dbo.SynergisticChampions", new[] { "FirstChampion_ChampionId" });
            DropIndex("dbo.RunesPages", new[] { "Rune_RuneId" });
            DropIndex("dbo.CounterSummonersSpells", new[] { "SummonersSpell_SummonersSpellId" });
            DropIndex("dbo.CounterSummonersSpells", new[] { "Champion_ChampionId" });
            DropIndex("dbo.CounterItems", new[] { "Item_ItemId" });
            DropIndex("dbo.CounterItems", new[] { "Champion_ChampionId" });
            DropIndex("dbo.CounterChampions", new[] { "CounteringChampion_ChampionId" });
            DropIndex("dbo.CounterChampions", new[] { "CounteredChampion_ChampionId" });
            DropColumn("dbo.SynergisticRunesPages", "RunesPage_RunesPageId");
            DropColumn("dbo.SynergisticRunesPages", "Champion_ChampionId");
            DropColumn("dbo.SynergisticMasteriesTrees", "MasteriesTree_MasteriesTreeId");
            DropColumn("dbo.SynergisticMasteriesTrees", "Champion_ChampionId");
            DropColumn("dbo.SynergisticItems", "Items_ItemId");
            DropColumn("dbo.SynergisticItems", "Champions_ChampionId");
            DropColumn("dbo.SynergisticChampions", "SecondChampion_ChampionId");
            DropColumn("dbo.SynergisticChampions", "FirstChampion_ChampionId");
            DropColumn("dbo.RunesPages", "Rune_RuneId");
            DropColumn("dbo.CounterSummonersSpells", "SummonersSpell_SummonersSpellId");
            DropColumn("dbo.CounterSummonersSpells", "Champion_ChampionId");
            DropColumn("dbo.CounterItems", "Item_ItemId");
            DropColumn("dbo.CounterItems", "Champion_ChampionId");
            DropColumn("dbo.CounterChampions", "CounteringChampion_ChampionId");
            DropColumn("dbo.CounterChampions", "CounteredChampion_ChampionId");
            DropTable("dbo.MasteriesTrees");
        }
    }
}

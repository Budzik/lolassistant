﻿using LolAssistant.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LolAssistant.Models.ViewModels
{
    public class RuneVM
    {
        public Rune Rune { get; set; }
        public int Count { get; set; }
        
    }
}

﻿using System;
using System.Runtime.Serialization;

namespace LolAssistant.Exceptions
{
    [Serializable]
    internal class MatchNotFoundException : Exception
    {
        public MatchNotFoundException()
        {
        }

        public MatchNotFoundException(string message) : base(message)
        {
        }

        public MatchNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected MatchNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
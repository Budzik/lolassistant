﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LolAssistant.Models.Entities
{
    public class SynergisticMasteriesTree
    {
        public int SynergisticMasteriesTreeId { get; set; }
        public string Description { get; set; }
        public virtual Champion Champion { get; set; }
        public virtual MasteriesTree MasteriesTree { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LolAssistant.Models.Entities
{
    public class SummonersSpell
    {
        public int SummonersSpellId { get; set; }
        public long LolSummonersSpellId { get; set; }
        public string Name { get; set; }
        public string Graphic { get; set; }
        public virtual IEnumerable<CounterSummonersSpell> CounterSummonersSpell { get; set; }
        public virtual IEnumerable<SynergisticSummonersSpell> SynergisticSummonersSpell { get; set; }
    }
}

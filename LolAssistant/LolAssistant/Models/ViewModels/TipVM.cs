﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LolAssistant.Models.ViewModels
{
    public class TipVM
    {
        public TipPositiveness tipPositiveness { get; set; }
        public string description { get; set; }
    }
}

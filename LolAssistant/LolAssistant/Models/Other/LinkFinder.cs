﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LolAssistant.Models.Other
{
    public static class LinkFinder
    {
        private static readonly string championGgLink = "https://www.champion.gg/champion/[Champion12343312]/";
        private static readonly string championLinkPart = "[Champion12343312]";
        private static readonly string opGgLink = "https://[server1919134].op.gg/summoner/userName=[name124599900]";
        private static readonly string serverPart = "[server1919134]";
        private static readonly string namePart = "[name124599900]";

        public static string getLinkToOpGg(string name, Servers server)
        {
            name = name.Replace(' ', '+');
            return opGgLink.Replace(serverPart, server.ToString()).Replace(namePart, name);
        }
        public static string getLinkToChampionGg(string championName)
        {
            championName = championName.Replace(" ", "");
            return championGgLink.Replace(championLinkPart, championName);
        }
    }
}

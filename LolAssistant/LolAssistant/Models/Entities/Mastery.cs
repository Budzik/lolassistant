﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LolAssistant.Models.Entities
{
    public class Mastery
    {
        public int MasteryId { get; set; }
        public long LolMasteryId { get; set; }
        public string Name { get; set; }
        public string Profit { get; set; }
        public string Graphic { get; set; }
        public ICollection<MasteriesTree> MasteriesTrees { get; set; }
        public MasteryTree MasteryTree { get; set; }
        public int HorizontalPosition { get; set; }
        public int VerticalPosition { get; set; }
        public Mastery()
        {
            MasteriesTrees = new HashSet<MasteriesTree>();
        }
    }
}

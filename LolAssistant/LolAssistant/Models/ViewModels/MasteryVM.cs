﻿using LolAssistant.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LolAssistant.Models.ViewModels
{
    public class MasteryVM
    {
        public Mastery mastery { get; set; }
        public int count { get; set; }
    }
}

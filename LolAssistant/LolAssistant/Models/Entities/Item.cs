﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LolAssistant.Models.Entities
{
    public class Item
    {
        public int ItemId { get; set; }
        public long LolItemId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Graphic { get; set; }
        public virtual IEnumerable<CounterItem> CounterItem { get; set; }
        public virtual IEnumerable<SynergisticItem> SynergisticItem { get; set; }
    }
}

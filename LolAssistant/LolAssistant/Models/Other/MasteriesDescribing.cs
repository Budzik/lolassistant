﻿using LolAssistant.Models.Entities;
using LolAssistant.Models.ViewModels;
using LolAssistant.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LolAssistant.Models.Other
{
    public static class MasteriesDescribing
    {
        public static string GetMasteriesDescribing(IEnumerable<MasteryVM> masteries)
        {
            int[] pointsInMasteriesTrees = new int[3];
            foreach (var item in masteries)
            {
                switch (item.mastery.MasteryTree)
                {
                    case MasteryTree.Ferocity:
                        pointsInMasteriesTrees[0] += item.count;
                        break;
                    case MasteryTree.Cunning:
                        pointsInMasteriesTrees[1] += item.count;
                        break;
                    case MasteryTree.Resolve:
                        pointsInMasteriesTrees[2] += item.count;
                        break;
                    default:
                        break;
                }
            }
            return pointsInMasteriesTrees[0].ToString() + "/" + pointsInMasteriesTrees[1].ToString() + "/" + pointsInMasteriesTrees[2].ToString();
        }

        public static bool IsMasteriesListContainMastary(IEnumerable<MasteryVM> masteries, Mastery mastery)
        {
            foreach (var item in masteries)
            {
                if(item.mastery.LolMasteryId == mastery.LolMasteryId)
                    return true;
            }
            return false;
        }
        public static Mastery GetMasteryByPosition(MasteryTree tree, int horizontalPosition, int verticalPosition)
        {
            MasteryService ms = new MasteryService();
            return ms.GetMasteryByPosition(tree, horizontalPosition, verticalPosition);
        }
        public static int GetMasteryCount(Mastery mastery, IEnumerable<MasteryVM> masteries)
        {
            foreach (var item in masteries)
            {
                if(mastery.LolMasteryId == item.mastery.LolMasteryId)
                {
                    return item.count;
                }

            }
            return -1;
        }
    }
}

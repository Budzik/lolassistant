﻿using LolAssistant.Models;
using LolAssistant.Models.Entities;
using LolAssistant.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LolAssistant.Services
{
    public class ChampionService
    {
        private LolAssistantContext Context;

        public ChampionService()
        {
            Context = new LolAssistantContext();
        }
        public IEnumerable<Champion> GetAllChampions()
        {
            return Context.Champions.ToList();
        }

        public Champion GetChampionByLolId(int id)
        {
            try
            {
                return (from c in Context.Champions where c.LolChampionId == id select c).ToList()[0];
            }
            catch (Exception e)
            {
                throw new ArgumentException("Champion with " + id.ToString() + " lol id not found.");
            }
        }
        public Champion GetChampionByName(string name)
        {
            try
            {
                return (from c in Context.Champions where c.Name.Equals(name) select c).ToList()[0];
            }
            catch(Exception e)
            {
                throw new ArgumentException("Champion with " + name + " not found.");
            }
        }
        public List<TipVM> GetTipsFromCounteredChampions(Champion currentChampion, Champion oppositeChampion)
        {
            List<TipVM> tips = new List<TipVM>();
            var negativeTips = GetCounterChampionsFromChampionId(currentChampion.ChampionId, oppositeChampion.ChampionId);
            var positiveTips = GetCounterChampionsFromChampionId(oppositeChampion.ChampionId, currentChampion.ChampionId);
            foreach (var item in negativeTips)
            {
                tips.Add(new TipVM() { description = item.Description, tipPositiveness = TipPositiveness.Negative });
            }
            foreach (var item in positiveTips)
            {
                tips.Add(new TipVM() { description = item.Description, tipPositiveness = TipPositiveness.Positive });
            }
            return tips;
        }
        private List<CounterChampion> GetCounterChampionsFromChampionId(int counteredId, int counteringId)
        {
            return (from c in Context.CounterChampions where c.CounteredChampion.ChampionId == counteredId && c.CounteringChampion.ChampionId == counteringId select c).ToList();
        }
        public void PutSomeCounterChampion()
        {
            PutCounterChampion("Zed", "Kayle", "Kayle R block damage from Zed.");
            PutCounterChampion("Caitlyn", "Sivir", "Sivir spellshield allow to ignore Caitlyn traps and ultimate.");
            PutCounterChampion("Caitlyn", "Braum", "Braum shield allow to ignore Caitlyn ultimate.");
            PutCounterChampion("Ezreal", "Braum", "Braum shield allow to block Ezreal global ultimate.");
            PutCounterChampion("Jinx", "Braum", "Braum shield allow to block Jinx global ultimate.");
            PutCounterChampion("Jinx", "Blitzcrank", "Jinx doesn't have any dash skill so Blitzcrank has easy way to grab her.");
            PutCounterChampion("Varus", "Blitzcrank", "Varus doesn't have any dash skill so Blitzcrank has easy way to grab him.");
            PutCounterChampion("Miss Fortune", "Blitzcrank", "Miss Fortune doesn't have any dash skill so Blitzcrank has easy way to grab her.");
            PutCounterChampion("Twitch", "Blitzcrank", "Twitch doesn't have any dash skill so Blitzcrank has easy way to grab him.");
            PutCounterChampion("Ashe", "Blitzcrank", "Ashe doesn't have any dash skill so Blitzcrank has easy way to grab her.");
            PutCounterChampion("Draven", "Blitzcrank", "Draven doesn't have any dash skill so Blitzcrank has easy way to grab him.");
            PutCounterChampion("Jinx", "Thresh", "Jinx doesn't have any dash skill so Thresh has easy way to grab her.");
            PutCounterChampion("Varus", "Thresh", "Varus doesn't have any dash skill so Thresh has easy way to grab him.");
            PutCounterChampion("Miss Fortune", "Thresh", "Miss Fortune doesn't have any dash skill so Thresh has easy way to grab her.");
            PutCounterChampion("Twitch", "Thresh", "Twitch doesn't have any dash skill so Thresh has easy way to grab him.");
            PutCounterChampion("Ashe", "Thresh", "Ashe doesn't have any dash skill so Thresh has easy way to grab her.");
            PutCounterChampion("Draven", "Thresh", "Draven doesn't have any dash skill so Thresh has easy way to grab him.");
            PutCounterChampion("Thresh", "Sivir", "Sivir spellshield ignore Thresh hook.");
            PutCounterChampion("Blitzcrank", "Sivir", "Sivir spellshield ignore Blitzcrank grab.");
            PutCounterChampion("Katarina", "Malzahar", "Malzahar ultimate and Q can interupt katarina ultimate.");
            PutCounterChampion("Sivir", "Vayne", "Vayne is hypercarry, so her mechanics is base on autoatacks. Sivir spellshield is useless against her.");
            PutCounterChampion("Garen", "Thresh", "Thresh has a lots of crowd control effects. It's good against Garen, becouse he is melee champion and you allow to deal damage for your adc.");
            PutCounterChampion("Pantheon", "Blitzcrank", "Blitzcrank can detach Pantheon from his team. He is not tanky and Blitzcrank team has good way to kill him.");
            PutCounterChampion("Amumu", "Sivir", "Sivir can block Ammumu crowd control effects by spellshield.");
            PutCounterChampion("Amumu", "Lee Sin", "Ammumu is weak in early game. Lee Sin should kill him at level 2-3 in his part of jungle.");
            PutCounterChampion("Malzahar", "Garen", "Garen can interupt Malzahar ultimate by his Q. Garen is tank, so it is not possible to focus on him in teamfight.");
            PutCounterChampion("Vayne", "Katarina", "Katarina deal a lots of damage in short time. She can kill vayne before her team reaction.");
            PutCounterChampion("Garen", "Pantheon", "Garen Q is strong, but it's autoatack and Pantheon has easy way to block it by his passive.");
            PutCounterChampion("Katarina", "Thresh", "Thresh has 2 skills, which can interrupt Katarina ultimate.");
            PutCounterChampion("Lee Sin", "Vayne", "Vayne can use her E to reject Lee Sin, to prevent kill on her.");
            PutCounterChampion("Malzahar", "Lee Sin", "Malzahar is immobile during his ultimate. Lee Sin is mobile champion and has easy way to kill him.");
            Context.SaveChanges();


        }
        public void PutCounterChampion(string countered, string countering, string desctription)
        {
            CounterChampion counter = new CounterChampion() { CounteredChampion = GetChampionByName(countered), CounteringChampion = GetChampionByName(countering), Description = desctription };
            Context.CounterChampions.Add(counter);
        }
    }
}

﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(LolAssistant.Startup))]
namespace LolAssistant
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

﻿using LolAssistant.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LolAssistant.Models.Other
{
    static public class RunesDescribing
    {
        static public string RunesDescription(IEnumerable<RuneVM> runes)
        {
            StringBuilder runesDescription = new StringBuilder();
            foreach (var item in runes)
            {
                runesDescription.Append((item.Count * item.Rune.Value).ToString() + " " + item.Rune.Profit + "\n");
            }
            return runesDescription.ToString();
        }
    }
}

﻿using LolAssistant.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LolAssistant.Models.Other
{
    static public class ImageFinder
    {
        private static readonly string imageUrl = "http://ddragon.leagueoflegends.com/cdn/7.10.1/img/";
        private static readonly string championPartOfUrl = "champion/";
        private static readonly string itemPartOfUrl = "item/";
        private static readonly string summonerSpellPartOfUrl = "spell/";
        private static readonly string runePartOfUrl = "rune/";
        private static readonly string masteryPartOfUrl = "mastery/";

        public static string GetChampionImageUrl(Champion champion)
        {
            return imageUrl + championPartOfUrl + champion.Graphic;
        }
        public static string GetItemImageUrl(Item item)
        {
            return imageUrl + itemPartOfUrl + item.Graphic;
        }
        public static string GetRuneImageUrl(Rune rune)
        {
            return imageUrl + runePartOfUrl + rune.Graphic;
        }
        public static string GetMasteryImageUrl(Mastery mastery)
        {
            return imageUrl + masteryPartOfUrl + mastery.Graphic;
        }
        public static string GetSummonerSpellImageUrl(SummonersSpell summonerSpell)
        {
            return imageUrl + summonerSpellPartOfUrl + summonerSpell.Graphic;
        }
        public static string GetGreyMasteryImageUrl(Mastery mastery)
        {
            return imageUrl + masteryPartOfUrl + "gray_" + mastery.Graphic;
        }
    }
}

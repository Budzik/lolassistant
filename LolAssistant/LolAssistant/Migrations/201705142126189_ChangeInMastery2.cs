namespace LolAssistant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeInMastery2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Masteries", "HorizontalPosition", c => c.Int(nullable: false));
            AddColumn("dbo.Masteries", "VerticalPosition", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Masteries", "VerticalPosition");
            DropColumn("dbo.Masteries", "HorizontalPosition");
        }
    }
}

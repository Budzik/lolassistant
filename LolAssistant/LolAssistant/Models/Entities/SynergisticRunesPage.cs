﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LolAssistant.Models.Entities
{
    public class SynergisticRunesPage
    {
        public int SynergisticRunesPageId { get; set; }
        public string Description { get; set; }
        public virtual Champion Champion { get; set; }
        public virtual RunesPage RunesPage { get; set; }
    }
}

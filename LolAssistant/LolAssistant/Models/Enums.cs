﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LolAssistant.Models
{
    public enum RuneType
    {
        Mark, Seal, Glyph, Quintessence
    }
    public enum Servers
    {
        EUNE, EUW, BR, LAN, LAS, NA, OCE, RU, TR, SEA, KR, PBE
    }
    public enum TeamSite
    {
        Blue, Red
    }
    public enum MasteryTree
    {
        Ferocity, Cunning, Resolve
    }
    public enum TipPositiveness
    {
        Positive, Neutral, Negative
    }
}

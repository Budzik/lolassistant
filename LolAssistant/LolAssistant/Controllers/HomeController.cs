﻿using LolAssistant.Exceptions;
using LolAssistant.Models;
using LolAssistant.Models.Entities;
using LolAssistant.Models.ViewModels;
using LolAssistant.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LolAssistant.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            RiotApiService ras = new RiotApiService();
            ChampionService cs = new ChampionService();
            /*ras.RefreshChampionList();
            ras.RefreshItemList();
            ras.RefreshSummonersSpellsList();
            ras.RefreshRunesList();
            ras.RefreshMasteriesList();*/
            //cs.PutSomeCounterChampion();

            return View();
        }
        
        [HttpGet]
        public ActionResult CheckNickName(string name, Servers server)
        {
            RiotApiService ras = new RiotApiService();
            if (name.Equals("121212SeCrEtPaSsWoRd121212"))
            {
                MatchVM match = ras.GetSecretTeam();
                return View("MatchFound", match);
            }
            else
            {

                try
                {
                    MatchVM match = ras.GetMatchBySummonerName(name, server);
                    return View("MatchFound", match);
                }
                catch (PlayerNotFoundException e)
                {
                    return View("PlayerNotFound");
                }
                catch (MatchNotFoundException e)
                {
                    return View("MatchNotFound");
                }
            }

        }
        public ActionResult MasteriesWindow(List<int> MasteriesId, List<int> MasteriesCount, TeamSite teamSite, int player)
        {
            MasteryService ms = new MasteryService();
            
            return PartialView("MasteriesWindow", ms.GetMasteriesById(MasteriesId, MasteriesCount));
        }
    }
}
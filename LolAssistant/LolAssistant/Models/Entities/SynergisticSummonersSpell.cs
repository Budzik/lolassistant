﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LolAssistant.Models.Entities
{
    public class SynergisticSummonersSpell
    {
        public int SynergisticSummonersSpellId { get; set; }
        public string Description { get; set; }
        public virtual Champion Champion { get; set; }
        public virtual SummonersSpell SummonersSpell { get; set; }
    }
}

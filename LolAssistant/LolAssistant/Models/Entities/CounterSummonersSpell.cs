﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LolAssistant.Models.Entities
{
    public class CounterSummonersSpell
    {
        public int CounterSummonersSpellId { get; set; }
        public string Description { get; set; }
        public virtual Champion Champion { get; set; }
        public virtual SummonersSpell SummonersSpell { get; set; }
    }
}

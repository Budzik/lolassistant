namespace LolAssistant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class start : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Champions",
                c => new
                    {
                        ChampionId = c.Int(nullable: false, identity: true),
                        LolChampionId = c.Long(nullable: false),
                        Name = c.String(),
                        Title = c.String(),
                        Graphic = c.String(),
                        CounterItems_CounterItemId = c.Int(),
                        CounterSummonersSpell_CounterSummonersSpellId = c.Int(),
                        SynergisticItems_SynergisticItemId = c.Int(),
                        SynergisticMasteriesTree_SynergisticMasteriesTreeId = c.Int(),
                        SynergisticRunesPages_SynergisticRunesPageId = c.Int(),
                        SynergisticSummonersSpells_SynergisticSummonersSpellId = c.Int(),
                    })
                .PrimaryKey(t => t.ChampionId)
                .ForeignKey("dbo.CounterItems", t => t.CounterItems_CounterItemId)
                .ForeignKey("dbo.CounterSummonersSpells", t => t.CounterSummonersSpell_CounterSummonersSpellId)
                .ForeignKey("dbo.SynergisticItems", t => t.SynergisticItems_SynergisticItemId)
                .ForeignKey("dbo.SynergisticMasteriesTrees", t => t.SynergisticMasteriesTree_SynergisticMasteriesTreeId)
                .ForeignKey("dbo.SynergisticRunesPages", t => t.SynergisticRunesPages_SynergisticRunesPageId)
                .ForeignKey("dbo.SynergisticSummonersSpells", t => t.SynergisticSummonersSpells_SynergisticSummonersSpellId)
                .Index(t => t.CounterItems_CounterItemId)
                .Index(t => t.CounterSummonersSpell_CounterSummonersSpellId)
                .Index(t => t.SynergisticItems_SynergisticItemId)
                .Index(t => t.SynergisticMasteriesTree_SynergisticMasteriesTreeId)
                .Index(t => t.SynergisticRunesPages_SynergisticRunesPageId)
                .Index(t => t.SynergisticSummonersSpells_SynergisticSummonersSpellId);
            
            CreateTable(
                "dbo.CounterItems",
                c => new
                    {
                        CounterItemId = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.CounterItemId);
            
            CreateTable(
                "dbo.CounterSummonersSpells",
                c => new
                    {
                        CounterSummonersSpellId = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.CounterSummonersSpellId);
            
            CreateTable(
                "dbo.SynergisticItems",
                c => new
                    {
                        SynergisticItemId = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.SynergisticItemId);
            
            CreateTable(
                "dbo.SynergisticMasteriesTrees",
                c => new
                    {
                        SynergisticMasteriesTreeId = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.SynergisticMasteriesTreeId);
            
            CreateTable(
                "dbo.SynergisticRunesPages",
                c => new
                    {
                        SynergisticRunesPageId = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.SynergisticRunesPageId);
            
            CreateTable(
                "dbo.SynergisticSummonersSpells",
                c => new
                    {
                        SynergisticSummonersSpellId = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.SynergisticSummonersSpellId);
            
            CreateTable(
                "dbo.CounterChampions",
                c => new
                    {
                        CounterChampionId = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.CounterChampionId);
            
            CreateTable(
                "dbo.Items",
                c => new
                    {
                        ItemId = c.Int(nullable: false, identity: true),
                        LolItemId = c.Long(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                        Graphic = c.String(),
                        CounterItems_CounterItemId = c.Int(),
                        SynergisticItems_SynergisticItemId = c.Int(),
                    })
                .PrimaryKey(t => t.ItemId)
                .ForeignKey("dbo.CounterItems", t => t.CounterItems_CounterItemId)
                .ForeignKey("dbo.SynergisticItems", t => t.SynergisticItems_SynergisticItemId)
                .Index(t => t.CounterItems_CounterItemId)
                .Index(t => t.SynergisticItems_SynergisticItemId);
            
            CreateTable(
                "dbo.Runes",
                c => new
                    {
                        RuneId = c.Int(nullable: false, identity: true),
                        LolRuneId = c.Long(nullable: false),
                        Name = c.String(),
                        Profit = c.String(),
                        Value = c.Single(nullable: false),
                        Type = c.Int(nullable: false),
                        Graphic = c.String(),
                    })
                .PrimaryKey(t => t.RuneId);
            
            CreateTable(
                "dbo.RunesPages",
                c => new
                    {
                        RunesPageId = c.Int(nullable: false, identity: true),
                        SynergisticRunesPages_SynergisticRunesPageId = c.Int(),
                    })
                .PrimaryKey(t => t.RunesPageId)
                .ForeignKey("dbo.SynergisticRunesPages", t => t.SynergisticRunesPages_SynergisticRunesPageId)
                .Index(t => t.SynergisticRunesPages_SynergisticRunesPageId);
            
            CreateTable(
                "dbo.SummonersSpells",
                c => new
                    {
                        SummonersSpellId = c.Int(nullable: false, identity: true),
                        LolSummonersSpellId = c.Long(nullable: false),
                        Name = c.String(),
                        Graphic = c.String(),
                        SynergisticMasteriesTree_SynergisticMasteriesTreeId = c.Int(),
                    })
                .PrimaryKey(t => t.SummonersSpellId)
                .ForeignKey("dbo.SynergisticMasteriesTrees", t => t.SynergisticMasteriesTree_SynergisticMasteriesTreeId)
                .Index(t => t.SynergisticMasteriesTree_SynergisticMasteriesTreeId);
            
            CreateTable(
                "dbo.SynergisticChampions",
                c => new
                    {
                        SynergisticChampionId = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.SynergisticChampionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SummonersSpells", "SynergisticMasteriesTree_SynergisticMasteriesTreeId", "dbo.SynergisticMasteriesTrees");
            DropForeignKey("dbo.RunesPages", "SynergisticRunesPages_SynergisticRunesPageId", "dbo.SynergisticRunesPages");
            DropForeignKey("dbo.Items", "SynergisticItems_SynergisticItemId", "dbo.SynergisticItems");
            DropForeignKey("dbo.Items", "CounterItems_CounterItemId", "dbo.CounterItems");
            DropForeignKey("dbo.Champions", "SynergisticSummonersSpells_SynergisticSummonersSpellId", "dbo.SynergisticSummonersSpells");
            DropForeignKey("dbo.Champions", "SynergisticRunesPages_SynergisticRunesPageId", "dbo.SynergisticRunesPages");
            DropForeignKey("dbo.Champions", "SynergisticMasteriesTree_SynergisticMasteriesTreeId", "dbo.SynergisticMasteriesTrees");
            DropForeignKey("dbo.Champions", "SynergisticItems_SynergisticItemId", "dbo.SynergisticItems");
            DropForeignKey("dbo.Champions", "CounterSummonersSpell_CounterSummonersSpellId", "dbo.CounterSummonersSpells");
            DropForeignKey("dbo.Champions", "CounterItems_CounterItemId", "dbo.CounterItems");
            DropIndex("dbo.SummonersSpells", new[] { "SynergisticMasteriesTree_SynergisticMasteriesTreeId" });
            DropIndex("dbo.RunesPages", new[] { "SynergisticRunesPages_SynergisticRunesPageId" });
            DropIndex("dbo.Items", new[] { "SynergisticItems_SynergisticItemId" });
            DropIndex("dbo.Items", new[] { "CounterItems_CounterItemId" });
            DropIndex("dbo.Champions", new[] { "SynergisticSummonersSpells_SynergisticSummonersSpellId" });
            DropIndex("dbo.Champions", new[] { "SynergisticRunesPages_SynergisticRunesPageId" });
            DropIndex("dbo.Champions", new[] { "SynergisticMasteriesTree_SynergisticMasteriesTreeId" });
            DropIndex("dbo.Champions", new[] { "SynergisticItems_SynergisticItemId" });
            DropIndex("dbo.Champions", new[] { "CounterSummonersSpell_CounterSummonersSpellId" });
            DropIndex("dbo.Champions", new[] { "CounterItems_CounterItemId" });
            DropTable("dbo.SynergisticChampions");
            DropTable("dbo.SummonersSpells");
            DropTable("dbo.RunesPages");
            DropTable("dbo.Runes");
            DropTable("dbo.Items");
            DropTable("dbo.CounterChampions");
            DropTable("dbo.SynergisticSummonersSpells");
            DropTable("dbo.SynergisticRunesPages");
            DropTable("dbo.SynergisticMasteriesTrees");
            DropTable("dbo.SynergisticItems");
            DropTable("dbo.CounterSummonersSpells");
            DropTable("dbo.CounterItems");
            DropTable("dbo.Champions");
        }
    }
}

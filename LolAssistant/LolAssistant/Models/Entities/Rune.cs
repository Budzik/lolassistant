﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LolAssistant.Models.Entities
{
    public class Rune
    {
        public int RuneId { get; set; }
        public long LolRuneId { get; set; }
        public string Name { get; set; }
        public string Profit { get; set; }
        public float Value { get; set; }
        public RuneType Type { get; set; }
        public string Graphic { get; set; }
        public int Tier { get; set; }
        public virtual ICollection<RunesPage> RunesPages { get; set; }
        public Rune()
        {
            RunesPages = new HashSet<RunesPage>();
        }
    }
}

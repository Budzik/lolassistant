﻿using LolAssistant.Models;
using LolAssistant.Models.Entities;
using LolAssistant.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LolAssistant.Services
{
    public class MasteryService
    {
        private LolAssistantContext Context;

        public MasteryService()
        {
            Context = new LolAssistantContext();
        }

        public IEnumerable<Mastery> GetAllMasteries()
        {
            return Context.Masteries.ToList();
        }
        public List<MasteryVM> GetMasteriesById(List<int> ids, List<int> counts)
        {
            if (ids != null && counts != null)
            {
                List<MasteryVM> masteries = new List<MasteryVM>();
                for (int i = 0; i < ids.Count; i++)
                {
                    Mastery mastery = GetMasteryByLolId(ids[i]);
                    masteries.Add(new MasteryVM() { mastery = mastery, count = counts[i] });
                }
                return masteries;
            }
            return new List<MasteryVM>();
        }
        public Mastery GetMasteryByLolId(int id)
        {
            try
            {
                return (from c in Context.Masteries where c.LolMasteryId == id select c).ToList()[0];
            }
            catch(Exception e)
            {
                throw new ArgumentException("Mastery with " + id.ToString() + " lol id not found.");
            }
        }
        public Mastery GetMasteryByPosition(MasteryTree tree, int horizontalPosition, int verticalPosition)
        {
            List<Mastery> masteries = Context.Masteries.ToList();
            horizontalPosition++;
            verticalPosition++;
            if (verticalPosition % 2 == 1)
            {
                if(horizontalPosition == 2)
                {
                    horizontalPosition = -1;
                }
                else if(horizontalPosition == 3)
                {
                    horizontalPosition = 2;
                }
            }
            foreach (var item in masteries)
            {
                if (item.MasteryTree == tree && horizontalPosition == item.HorizontalPosition && verticalPosition == item.VerticalPosition)
                {
                    return item;
                }
            }
            return null;
        }
    }
}

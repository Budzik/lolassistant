﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LolAssistant.Models.Entities
{
    public class Champion
    {
        public int ChampionId { get; set; }
        public long LolChampionId { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Graphic { get; set; }
        public virtual IEnumerable<CounterItem> CounterItem { get; set; }
        public virtual IEnumerable<SynergisticItem> SynergisticItem { get; set; }
        public virtual IEnumerable<CounterSummonersSpell> CounterSummonersSpell { get; set; }
        public virtual IEnumerable<SynergisticSummonersSpell> SynergisticSummonersSpell { get; set; }
        public virtual IEnumerable<SynergisticRunesPage> SynergisticRunesPage { get; set; }
        public virtual IEnumerable<SynergisticMasteriesTree> SynergisticMasteriesTree { get; set; }

    }
}

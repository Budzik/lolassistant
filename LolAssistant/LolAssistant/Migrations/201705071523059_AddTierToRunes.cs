namespace LolAssistant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTierToRunes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Runes", "Tier", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Runes", "Tier");
        }
    }
}

namespace LolAssistant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangesInMasteryEntity : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Masteries", "MasteryTree", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Masteries", "MasteryTree");
        }
    }
}

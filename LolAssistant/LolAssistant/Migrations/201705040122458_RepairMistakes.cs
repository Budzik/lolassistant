namespace LolAssistant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RepairMistakes : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Champions", "CounterItems_CounterItemId", "dbo.CounterItems");
            DropForeignKey("dbo.Champions", "CounterSummonersSpell_CounterSummonersSpellId", "dbo.CounterSummonersSpells");
            DropForeignKey("dbo.Champions", "SynergisticItems_SynergisticItemId", "dbo.SynergisticItems");
            DropForeignKey("dbo.Champions", "SynergisticMasteriesTree_SynergisticMasteriesTreeId", "dbo.SynergisticMasteriesTrees");
            DropForeignKey("dbo.Champions", "SynergisticRunesPages_SynergisticRunesPageId", "dbo.SynergisticRunesPages");
            DropForeignKey("dbo.Champions", "SynergisticSummonersSpells_SynergisticSummonersSpellId", "dbo.SynergisticSummonersSpells");
            DropForeignKey("dbo.Items", "CounterItems_CounterItemId", "dbo.CounterItems");
            DropForeignKey("dbo.Items", "SynergisticItems_SynergisticItemId", "dbo.SynergisticItems");
            DropForeignKey("dbo.RunesPages", "SynergisticRunesPages_SynergisticRunesPageId", "dbo.SynergisticRunesPages");
            DropForeignKey("dbo.SummonersSpells", "SynergisticMasteriesTree_SynergisticMasteriesTreeId", "dbo.SynergisticMasteriesTrees");
            DropIndex("dbo.Champions", new[] { "CounterItems_CounterItemId" });
            DropIndex("dbo.Champions", new[] { "CounterSummonersSpell_CounterSummonersSpellId" });
            DropIndex("dbo.Champions", new[] { "SynergisticItems_SynergisticItemId" });
            DropIndex("dbo.Champions", new[] { "SynergisticMasteriesTree_SynergisticMasteriesTreeId" });
            DropIndex("dbo.Champions", new[] { "SynergisticRunesPages_SynergisticRunesPageId" });
            DropIndex("dbo.Champions", new[] { "SynergisticSummonersSpells_SynergisticSummonersSpellId" });
            DropIndex("dbo.Items", new[] { "CounterItems_CounterItemId" });
            DropIndex("dbo.Items", new[] { "SynergisticItems_SynergisticItemId" });
            DropIndex("dbo.RunesPages", new[] { "SynergisticRunesPages_SynergisticRunesPageId" });
            DropIndex("dbo.SummonersSpells", new[] { "SynergisticMasteriesTree_SynergisticMasteriesTreeId" });
            DropColumn("dbo.Champions", "CounterItems_CounterItemId");
            DropColumn("dbo.Champions", "CounterSummonersSpell_CounterSummonersSpellId");
            DropColumn("dbo.Champions", "SynergisticItems_SynergisticItemId");
            DropColumn("dbo.Champions", "SynergisticMasteriesTree_SynergisticMasteriesTreeId");
            DropColumn("dbo.Champions", "SynergisticRunesPages_SynergisticRunesPageId");
            DropColumn("dbo.Champions", "SynergisticSummonersSpells_SynergisticSummonersSpellId");
            DropColumn("dbo.Items", "CounterItems_CounterItemId");
            DropColumn("dbo.Items", "SynergisticItems_SynergisticItemId");
            DropColumn("dbo.RunesPages", "SynergisticRunesPages_SynergisticRunesPageId");
            DropColumn("dbo.SummonersSpells", "SynergisticMasteriesTree_SynergisticMasteriesTreeId");
            DropTable("dbo.SynergisticSummonersSpells");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.SynergisticSummonersSpells",
                c => new
                    {
                        SynergisticSummonersSpellId = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.SynergisticSummonersSpellId);
            
            AddColumn("dbo.SummonersSpells", "SynergisticMasteriesTree_SynergisticMasteriesTreeId", c => c.Int());
            AddColumn("dbo.RunesPages", "SynergisticRunesPages_SynergisticRunesPageId", c => c.Int());
            AddColumn("dbo.Items", "SynergisticItems_SynergisticItemId", c => c.Int());
            AddColumn("dbo.Items", "CounterItems_CounterItemId", c => c.Int());
            AddColumn("dbo.Champions", "SynergisticSummonersSpells_SynergisticSummonersSpellId", c => c.Int());
            AddColumn("dbo.Champions", "SynergisticRunesPages_SynergisticRunesPageId", c => c.Int());
            AddColumn("dbo.Champions", "SynergisticMasteriesTree_SynergisticMasteriesTreeId", c => c.Int());
            AddColumn("dbo.Champions", "SynergisticItems_SynergisticItemId", c => c.Int());
            AddColumn("dbo.Champions", "CounterSummonersSpell_CounterSummonersSpellId", c => c.Int());
            AddColumn("dbo.Champions", "CounterItems_CounterItemId", c => c.Int());
            CreateIndex("dbo.SummonersSpells", "SynergisticMasteriesTree_SynergisticMasteriesTreeId");
            CreateIndex("dbo.RunesPages", "SynergisticRunesPages_SynergisticRunesPageId");
            CreateIndex("dbo.Items", "SynergisticItems_SynergisticItemId");
            CreateIndex("dbo.Items", "CounterItems_CounterItemId");
            CreateIndex("dbo.Champions", "SynergisticSummonersSpells_SynergisticSummonersSpellId");
            CreateIndex("dbo.Champions", "SynergisticRunesPages_SynergisticRunesPageId");
            CreateIndex("dbo.Champions", "SynergisticMasteriesTree_SynergisticMasteriesTreeId");
            CreateIndex("dbo.Champions", "SynergisticItems_SynergisticItemId");
            CreateIndex("dbo.Champions", "CounterSummonersSpell_CounterSummonersSpellId");
            CreateIndex("dbo.Champions", "CounterItems_CounterItemId");
            AddForeignKey("dbo.SummonersSpells", "SynergisticMasteriesTree_SynergisticMasteriesTreeId", "dbo.SynergisticMasteriesTrees", "SynergisticMasteriesTreeId");
            AddForeignKey("dbo.RunesPages", "SynergisticRunesPages_SynergisticRunesPageId", "dbo.SynergisticRunesPages", "SynergisticRunesPageId");
            AddForeignKey("dbo.Items", "SynergisticItems_SynergisticItemId", "dbo.SynergisticItems", "SynergisticItemId");
            AddForeignKey("dbo.Items", "CounterItems_CounterItemId", "dbo.CounterItems", "CounterItemId");
            AddForeignKey("dbo.Champions", "SynergisticSummonersSpells_SynergisticSummonersSpellId", "dbo.SynergisticSummonersSpells", "SynergisticSummonersSpellId");
            AddForeignKey("dbo.Champions", "SynergisticRunesPages_SynergisticRunesPageId", "dbo.SynergisticRunesPages", "SynergisticRunesPageId");
            AddForeignKey("dbo.Champions", "SynergisticMasteriesTree_SynergisticMasteriesTreeId", "dbo.SynergisticMasteriesTrees", "SynergisticMasteriesTreeId");
            AddForeignKey("dbo.Champions", "SynergisticItems_SynergisticItemId", "dbo.SynergisticItems", "SynergisticItemId");
            AddForeignKey("dbo.Champions", "CounterSummonersSpell_CounterSummonersSpellId", "dbo.CounterSummonersSpells", "CounterSummonersSpellId");
            AddForeignKey("dbo.Champions", "CounterItems_CounterItemId", "dbo.CounterItems", "CounterItemId");
        }
    }
}

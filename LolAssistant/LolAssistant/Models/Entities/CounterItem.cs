﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LolAssistant.Models.Entities
{
    public class CounterItem
    {
        public int CounterItemId { get; set; }
        public string Description { get; set; }
        public virtual Champion Champion { get; set; }
        public virtual Item Item { get; set; }
    }
}

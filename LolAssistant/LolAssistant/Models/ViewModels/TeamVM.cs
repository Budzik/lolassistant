﻿using LolAssistant.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LolAssistant.Models.ViewModels
{
    public class TeamVM
    {
        public IEnumerable<SummonerVM> Summoners { get; set; }
        public IEnumerable<Champion> Bans { get; set; }
        public TeamSite TeamSite { get; set; }
        public TeamVM()
        {
            Summoners = new List<SummonerVM>();
            Bans = new List<Champion>();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LolAssistant.Models.Entities
{
    public class MasteriesTree
    {
        public int MasteriesTreeId { get; set; }
        public virtual ICollection<Mastery> Masteries { get; set; }
        public virtual ICollection<SynergisticMasteriesTree> SynergisticMasteriesTree { get; set; }
        public MasteriesTree()
        {
            Masteries = new HashSet<Mastery>();
        }
    }
}

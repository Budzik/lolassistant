namespace LolAssistant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class repairDatabase4 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Masteries",
                c => new
                    {
                        MasteryId = c.Int(nullable: false, identity: true),
                        LolMasteryId = c.Long(nullable: false),
                        Name = c.String(),
                        Profit = c.String(),
                        Graphic = c.String(),
                    })
                .PrimaryKey(t => t.MasteryId);
            
            CreateTable(
                "dbo.RunesPageRunes",
                c => new
                    {
                        RunesPage_RunesPageId = c.Int(nullable: false),
                        Rune_RuneId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.RunesPage_RunesPageId, t.Rune_RuneId })
                .ForeignKey("dbo.RunesPages", t => t.RunesPage_RunesPageId, cascadeDelete: true)
                .ForeignKey("dbo.Runes", t => t.Rune_RuneId, cascadeDelete: true)
                .Index(t => t.RunesPage_RunesPageId)
                .Index(t => t.Rune_RuneId);
            
            CreateTable(
                "dbo.MasteryMasteriesTrees",
                c => new
                    {
                        Mastery_MasteryId = c.Int(nullable: false),
                        MasteriesTree_MasteriesTreeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Mastery_MasteryId, t.MasteriesTree_MasteriesTreeId })
                .ForeignKey("dbo.Masteries", t => t.Mastery_MasteryId, cascadeDelete: true)
                .ForeignKey("dbo.MasteriesTrees", t => t.MasteriesTree_MasteriesTreeId, cascadeDelete: true)
                .Index(t => t.Mastery_MasteryId)
                .Index(t => t.MasteriesTree_MasteriesTreeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MasteryMasteriesTrees", "MasteriesTree_MasteriesTreeId", "dbo.MasteriesTrees");
            DropForeignKey("dbo.MasteryMasteriesTrees", "Mastery_MasteryId", "dbo.Masteries");
            DropForeignKey("dbo.RunesPageRunes", "Rune_RuneId", "dbo.Runes");
            DropForeignKey("dbo.RunesPageRunes", "RunesPage_RunesPageId", "dbo.RunesPages");
            DropIndex("dbo.MasteryMasteriesTrees", new[] { "MasteriesTree_MasteriesTreeId" });
            DropIndex("dbo.MasteryMasteriesTrees", new[] { "Mastery_MasteryId" });
            DropIndex("dbo.RunesPageRunes", new[] { "Rune_RuneId" });
            DropIndex("dbo.RunesPageRunes", new[] { "RunesPage_RunesPageId" });
            DropTable("dbo.MasteryMasteriesTrees");
            DropTable("dbo.RunesPageRunes");
            DropTable("dbo.Masteries");
        }
    }
}

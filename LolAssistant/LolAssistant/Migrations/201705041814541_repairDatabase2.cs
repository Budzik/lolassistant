namespace LolAssistant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class repairDatabase2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.RunesPages", "Rune_RuneId", "dbo.Runes");
            DropIndex("dbo.RunesPages", new[] { "Rune_RuneId" });
            DropColumn("dbo.RunesPages", "Rune_RuneId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.RunesPages", "Rune_RuneId", c => c.Int());
            CreateIndex("dbo.RunesPages", "Rune_RuneId");
            AddForeignKey("dbo.RunesPages", "Rune_RuneId", "dbo.Runes", "RuneId");
        }
    }
}

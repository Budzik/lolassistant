﻿using LolAssistant.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LolAssistant.Models
{
    class LolAssistantContext : DbContext 
    {
        public LolAssistantContext() : base("LolAssistant database")
        {

        }
            
        public DbSet<Champion> Champions { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<SummonersSpell> SummonersSpells { get; set; }
        public DbSet<Rune> Runes { get; set; }
        public DbSet<RunesPage> RunesPages { get; set; }
        public DbSet<SynergisticChampion> SynergisticChampions { get; set; }
        public DbSet<SynergisticItem> SynergisticItems { get; set; }
        public DbSet<SynergisticMasteriesTree> SynergisticMasteriesTrees { get; set; }
        public DbSet<SynergisticRunesPage> SynergisticRunesPages { get; set; }
        public DbSet<CounterChampion> CounterChampions { get; set; }
        public DbSet<CounterItem> CounterItem { get; set; }
        public DbSet<CounterSummonersSpell> CounterSummonersSpells { get; set; }
        public DbSet<Mastery> Masteries { get; set; }
        public DbSet<MasteriesTree> MasteriesTrees { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

    }
}

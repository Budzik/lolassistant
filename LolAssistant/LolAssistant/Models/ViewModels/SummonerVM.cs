﻿using LolAssistant.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LolAssistant.Models.ViewModels
{
    public class SummonerVM
    {
        public Champion Champion { get; set; }
        public bool IsItYou { get; set; }
        public SummonersSpell SumSpell1 { get; set; }
        public SummonersSpell SumSpell2 { get; set; }
        public IEnumerable<RuneVM> Runes { get; set; }
        public IEnumerable<MasteryVM> Masteries { get; set; }
        public string Name { get; set; }
    }
}

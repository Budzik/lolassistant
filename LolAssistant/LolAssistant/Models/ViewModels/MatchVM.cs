﻿using LolAssistant.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LolAssistant.Models.ViewModels
{
    public class MatchVM
    {
        public TeamVM Team1 { get; set; }
        public TeamVM Team2 { get; set; }
        public Servers server { get; set; }
        public List<TipVM> tips { get; set; }
    }
}
